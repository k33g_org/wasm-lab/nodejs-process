
const fork = require('child_process').fork
childProcess01 = fork("./child-process.js")
childProcess02 = fork("./child-process.js")


// TODO: send "init" with the name of the function


childProcess01.send({
  text: "🤖> Hello from Mum"
})

childProcess02.send({
  text: "🤖> Hello from Mum"
})

// why once (si je n'attends qu'une seule réponse ?)
// je n'écoute qu'une seule fois
childProcess01.once("message", (message) => {
  console.log("🤖> Received by Mum", message)
})

// why once
childProcess02.once("message", (message) => {
  console.log("🤖> Received by Mum", message)
})

// why once (si je n'attends qu'une seule réponse ?)
childProcess01.on("message", (message) => {
  console.log("🤖👋> Received by Mum", message)
})

// why once
childProcess02.on("message", (message) => {
  console.log("🤖👋> Received by Mum", message)
})


