
const fancy = require('./fancy')

const childName = fancy.fancyName()

global.init = (params) => {
  console.log("👋", "Init", params)
}

// process.on("message", async (message) => {
process.on("message", (message) => {

  console.log(`🐥> message received from (Mum)parent process:`, message)

  // answer
  process.send({
    text: "🐥> 👋 hello from Child",
    pid: process.pid,
    name: childName
  })

  // Add a loop and send messages
  setInterval(() =>{ 
    process.send({
      text: `ping by ${process.pid} aka ${childName}`
    })
  }, 2000) //run this thang every 2 seconds

})

// TODO: check if this is tiggered when the process is killed
process.on("exit", (code) => {
  console.log(`🐥> exit code:`, code)
})