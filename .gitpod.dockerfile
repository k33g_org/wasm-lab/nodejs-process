FROM gitpod/workspace-full

RUN sudo apt-get update && \
    sudo apt-get install gettext -y

USER gitpod

RUN brew install exa && \
    brew install bat && \
    brew install httpie && \
    brew install hey

